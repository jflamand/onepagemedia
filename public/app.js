$(document).ready(function(){
    slider.init();
    slider.autoSlider();
});

slider = {
    init : function(){

        slider.elem = $('.sliderListContent');
        slider.nbSlide = slider.elem.find("img").length;
        slider.current = 0;

        $('#sliderArrow_right').click(function(){
            slider.next();

        });

        $('#sliderArrow_left').click(function(){
            slider.prev();

        }); 
        
        $('#sliderContent').mouseover(function(){
            slider.stopSlider();

        }); 

    },

    next : function (){
        slider.current++;
        if(slider.current > slider.nbSlide - 1){
            slider.current=0;
            slider.elem.stop().animate({marginLeft : "0px"});
           
        }

        else{
            slider.elem.stop().animate({marginLeft : -slider.current*700 + "px"});
        }

    },

    prev : function (){
        slider.current--;
        if(slider.current < 0){
            slider.current = slider.nbSlide-1;
           
        }

        
            slider.elem.stop().animate({marginLeft : -slider.current*700 + "px"});
        

    },

    autoSlider : function(){
        slider.timer = window.setInterval("slider.next()", 4000);

    },

    stopSlider : function(){
        window.clearInterval(slider.timer);

    }
}